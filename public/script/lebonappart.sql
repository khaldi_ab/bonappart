-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2015 at 07:27 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lebonappart`
--

-- --------------------------------------------------------

--
-- Table structure for table `annonces`
--

CREATE TABLE IF NOT EXISTS `annonces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `titre` varchar(128) NOT NULL,
  `description` varchar(500) NOT NULL,
  `idtype` int(11) NOT NULL,
  `idmode` int(11) NOT NULL,
  `prix` decimal(15,2) NOT NULL,
  `nbpiece` int(10) NOT NULL,
  `idville` int(11) NOT NULL,
  `idquartier` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fk_iduser` (`iduser`),
  KEY `fk_idtype` (`idtype`),
  KEY `fk_idmode` (`idmode`),
  KEY `fk_idquartier` (`idquartier`),
  KEY `fk_idvilleannonce` (`idville`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `annonces`
--

INSERT INTO `annonces` (`id`, `iduser`, `titre`, `description`, `idtype`, `idmode`, `prix`, `nbpiece`, `idville`, `idquartier`, `updated_at`, `created_at`) VALUES
(20, 1, 'Appartement F2', 'etc etc', 2, 2, '500.00', 2, 2, 2, '2015-11-02 06:24:02', '2015-11-02 06:24:02');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idannonce` int(11) NOT NULL,
  `mail` varchar(256) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `objet` varchar(256) NOT NULL,
  `body` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_message` (`idannonce`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `idannonce`, `mail`, `tel`, `objet`, `body`, `created_at`, `updated_at`) VALUES
(4, 20, 'utilisateu@immo.fr', '0673948625', 'Demande d&#39;Information', 'J&#39;ai besoin de plus d&#39;infos', '2015-11-02 06:25:40', '2015-11-02 06:25:40');

-- --------------------------------------------------------

--
-- Table structure for table `mode`
--

CREATE TABLE IF NOT EXISTS `mode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modename` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `mode`
--

INSERT INTO `mode` (`id`, `modename`) VALUES
(2, 'Location'),
(3, 'Vente');

-- --------------------------------------------------------

--
-- Table structure for table `quartier`
--

CREATE TABLE IF NOT EXISTS `quartier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quartiername` varchar(128) DEFAULT NULL,
  `idville` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_idville` (`idville`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `quartier`
--

INSERT INTO `quartier` (`id`, `quartiername`, `idville`) VALUES
(2, 'Rue de Turique', 2),
(3, 'Rue de Metz', 2),
(4, 'Rue Général Leclair', 3),
(5, 'Rue Général Foch', 3),
(6, 'Rue de paris', 2);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typename` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `typename`) VALUES
(2, 'Appartement'),
(3, 'Studio'),
(4, 'Maison'),
(5, 'Terrain'),
(6, 'Parking');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `tel` varchar(128) NOT NULL,
  `mail` varchar(128) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `tel`, `mail`, `updated_at`, `created_at`) VALUES
(1, 'admin', 'admin', '0632159821', 'admin@immo.fr', '2015-11-02 05:40:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ville`
--

CREATE TABLE IF NOT EXISTS `ville` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `villename` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ville`
--

INSERT INTO `ville` (`id`, `villename`) VALUES
(2, 'Nancy'),
(3, 'Metz');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `annonces`
--
ALTER TABLE `annonces`
  ADD CONSTRAINT `fk_idmode` FOREIGN KEY (`idmode`) REFERENCES `mode` (`id`),
  ADD CONSTRAINT `fk_idquartier` FOREIGN KEY (`idquartier`) REFERENCES `quartier` (`id`),
  ADD CONSTRAINT `fk_idtype` FOREIGN KEY (`idtype`) REFERENCES `type` (`id`),
  ADD CONSTRAINT `fk_iduser` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_idvilleannonce` FOREIGN KEY (`idville`) REFERENCES `ville` (`id`);

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_message` FOREIGN KEY (`idannonce`) REFERENCES `annonces` (`id`);

--
-- Constraints for table `quartier`
--
ALTER TABLE `quartier`
  ADD CONSTRAINT `fk_idville` FOREIGN KEY (`idville`) REFERENCES `ville` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
