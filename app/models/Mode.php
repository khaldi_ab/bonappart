<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Mode extends Eloquent {
	protected $table = 'mode';
	protected $key='id';

  public function annonce()
  {
    return $this->hasMany('Annonce');
  }

  public static function listeMode()
  {
  	return Mode::all();
  }
}

?>
