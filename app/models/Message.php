<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Message extends Eloquent {
	protected $table = 'message';
	protected $key='id';
	protected $timestamp=true;



  public static function envoyerMail( $id, $mail, $tel,$objet, $body)
  {
    $message= new Message();
    $message->idannonce=$id;
		$message->mail=$mail;
    $message->tel=$tel;
    $message->objet=$objet;
    $message->body=$body;

    $message->save();

  }
  
}

?>
