<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Ville extends Eloquent {
	protected $table = 'ville';
	protected $key='id';

  public function quartier()
  {
    return $this->hasMany('Quartier');
  }

  public static function listeVille()
  {
  	return Ville::all();
  }

}

?>
