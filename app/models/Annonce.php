<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Annonce extends Eloquent {
	protected $table = 'annonces';
	protected $key='id';
	protected $timestamp=true;

  public function user()
  {
    return $this->belongsTo('User','iduser','id');
  }

  public function type()
  {
    return $this->belongsTo('Type','idtype','id');
  }

  public function mode()
  {
    return $this->belongsTo('Mode','idmode','id');
  }

  public function ville()
  {
    return $this->belongsTo('Ville','idville','id');
  }

  public function quartier()
  {
    return $this->belongsTo('Quartier','idquartier','id');
  }

  public static function ajouterAnnonce( $titre, $description, $type,$mode, $prix, $nb, $ville, $quartier)
  {
    $annonce= new Annonce();
    $annonce->iduser= 1;
    $annonce->titre=$titre;
		$annonce->description=$description;
    $annonce->idtype=$type;
    $annonce->idmode=$mode;
    $annonce->prix=$prix;
    $annonce->nbpiece=$nb;
    $annonce->idville=$ville;
    $annonce->idquartier=$quartier;

    $annonce->save();

  }
  public static function recherche($titre,$type,$mode,$prix,$ville)
  {
    $ann= Annonce::where('titre','like','%'.$type);

    if($type!=''){
      $ann->where('idtype','=',$type);
    }
    if($mode!=''){
      $ann->where('idmode','=',$mode);
    }
    if($prix!=''){
      $ann->where('prix','<=',$prix);
    }
    if($ville!=''){
      $ann->where('idville','=',$ville);
    }

    if(isset($ann))
    {
      $annonce=$ann->orderBy('created_at','desc')->get();
      $res=array();


      foreach ($annonce as $a) {

        $r= array('id' =>$a->id ,'titre'=>$a->titre,'description'=>$a->description, 'type'=>$a->type()->first()->typename,'mode'=>$a->mode()->first()->modename,'ville'=>$a->ville()->first()->villename, 'quartier'=>$a->quartier()->first()->quartiername,'date'=>$a->updated_at,'prix'=>$a->prix);
        array_push($res, $r);
      }
      return $res;
    }
      else
        return '';
  }

}

?>
