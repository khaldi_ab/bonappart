<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Type extends Eloquent {
	protected $table = 'type';
	protected $key='id';
	protected $timestamp=false;

  public function annonce()
  {
    return $this->hasMany('Annonce');
  }
  public static function listeType()
  {
  	return Type::all();
  }

}

?>
