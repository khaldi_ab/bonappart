<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Quartier extends Eloquent {
	protected $table = 'quartier';
	protected $key='id';

  public function annonce()
  {
    return $this->hasMany('Annonce');
  }

  public function ville()
  {
    return $this->belongsTo('Ville');
  }
	public static function listeQuartier()
	{
		return Quartier::all();
	}
}

?>
