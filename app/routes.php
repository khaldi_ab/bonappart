<?php

// DB configuration
Configuration::config();

// Page d'accueil

$app->render('header.twig');

$app->get('/', function () use ($app) {
  $app->redirect('accueil');
})->name("home");

$app->get('/accueil', function () use ($app) {

	$v=Annonce::recherche( '','','', '', '');

	$app->render('accueil.twig', ['annonces'=>$v]);

})->name("home");

$app->get('/depot', function() use ($app) {

	$ville=Ville::listeVille();
	$mode=Mode::listeMode();
	$type=Type::listeType();
	$quartier=Quartier::listeQuartier();

 $app->render('depot.twig',['villes'=>$ville,'modes'=>$mode,'types'=>$type, 'quartiers'=>$quartier]);
})->name('result');

$app->post('/ajouter', function() use ($app) {
	try {
					$titre=filter_var($app->request->post('titre'),FILTER_SANITIZE_STRING);
					$type=filter_var($app->request->post('type'),FILTER_SANITIZE_STRING) ;
					$description=filter_var($app->request->post('description'),FILTER_SANITIZE_STRING) ;
					$mode=filter_var($app->request->post('mode'),FILTER_SANITIZE_STRING) ;
					$prix=filter_var($app->request->post('prix'),FILTER_SANITIZE_STRING) ;
					$nb=filter_var($app->request->post('nb'),FILTER_SANITIZE_STRING) ;
					$ville=filter_var($app->request->post('ville'),FILTER_SANITIZE_STRING) ;
					$quartier=filter_var($app->request->post('quartier'),FILTER_SANITIZE_STRING) ;
					Annonce::ajouterAnnonce( $titre,$description,$type,$mode, $prix, $nb, $ville, $quartier);

					$v=Annonce::recherche( '','','', '', '');

	        $app->render('accueil.twig', ['ajout'=>$titre,'annonces'=>$v]);
	  } catch(\Exception $e) {
	        $app->render('depot.twig', ['ajout'=>'']);
	  }

})->name('ajout');

$app->get('/recherche', function () use ($app) {
	$ville=Ville::listeVille();
	$mode=Mode::listeMode();
	$type=Type::listeType();

    $app->render('recherche.twig',['villes'=>$ville,'modes'=>$mode,'types'=>$type]);
})->name("recherche");

$app->post('/resultats', function() use ($app) {
	$ttre=filter_var($app->request->post('titre'),FILTER_SANITIZE_STRING);
	$tpe=filter_var($app->request->post('type'),FILTER_SANITIZE_STRING) ;
	$mde=filter_var($app->request->post('mode'),FILTER_SANITIZE_STRING) ;
	$pix=filter_var($app->request->post('prix'),FILTER_SANITIZE_STRING) ;
	$vlle=filter_var($app->request->post('ville'),FILTER_SANITIZE_STRING) ;

	$v=Annonce::recherche( $ttre,$tpe,$mde, $pix, $vlle);

	$ville=Ville::listeVille();
	$mode=Mode::listeMode();
	$type=Type::listeType();

 $app->render('recherche.twig',['villes'=>$ville,'modes'=>$mode,'types'=>$type,'annonces'=>$v]);
})->name('result');

$app->post('/contact', function () use ($app) {
		$id=filter_var($app->request->post('id'),FILTER_SANITIZE_STRING);
    $app->render('contact.twig',['id'=>$id]);
})->name("contacter");

$app->post('/envoyer', function () use ($app) {

		$id=filter_var($app->request->post('id'),FILTER_SANITIZE_STRING);
		$mail=filter_var($app->request->post('mail'),FILTER_SANITIZE_STRING);
		$tel=filter_var($app->request->post('tel'),FILTER_SANITIZE_STRING);
		$objet=filter_var($app->request->post('objet'),FILTER_SANITIZE_STRING);
		$body=filter_var($app->request->post('body'),FILTER_SANITIZE_STRING);

		Message::envoyerMail( $id, $mail, $tel,$objet, $body);
    $v=Annonce::recherche( '','','', '', '');
    $app->render('accueil.twig',['envoi'=>'reussi','annonces'=>$v]);
})->name("envoyer");
 ?>
